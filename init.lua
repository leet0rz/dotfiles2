-- ERROR CHECKER
--require("scripts.errorcheck.checker")

-- 0=============================================================================================0
-- preload tables
require("resources")
-- colorscheme for when errors occur
vim.cmd("colorscheme habamax")

-- load settings/remaps
require("settings_remaps")

-- load scripts
require("scripts.tabline")
require("scripts.autocmd")

-- load plugins
require("plugins")

-- after
require("scripts.statusline")
-- load all the plugin's remaps
require("settings_remaps").plugin_remaps()

