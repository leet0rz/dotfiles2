
-- Custom tabline configuration
vim.o.showtabline = 1 -- Always show the tabline

-- Function to generate the custom tabline
function custom_tabline()
    local tabline = ''
    local tabpages = vim.fn.gettabinfo()

    for i, tabpage in ipairs(tabpages) do
        local current = (i == vim.fn.tabpagenr())
        local tabname = tabpage.tabnr
        local winnr = tabpage.windows[1]

        -- Get the buffer name without the path
        local bufnr = vim.fn.winbufnr(winnr)
        local bufname = vim.fn.fnamemodify(vim.fn.bufname(bufnr), ':t')

        tabline = tabline .. '%' .. tabname .. 'T ' -- Tab number
        if current then
            tabline = tabline .. '%#TabLineSel# '
            -- tabline = color_string("white", tabline)
        else
            tabline = tabline .. '%#TabLine# '
        end
        tabline = tabline .. bufname .. ' '
    end

    tabline = tabline .. '%#TabLineFill#'
    return tabline
end

function tabline_render()
    local tabline_colorpick = "aqua"
    return table.concat({
        custom_tabline(),

    })
end

-- Set the tabline to use the custom format
vim.o.tabline = "%{%v:lua.tabline_render()%}"
