-- Autosave
vim.api.nvim_create_autocmd({ "FocusLost", "BufLeave" }, {
    callback = function()
        local buftype = vim.bo.buftype

        if buftype ~= 'nofile' and buftype ~= 'acwrite' then
            -- vim.cmd("silent wa")
            vim.cmd("silent! w")
        end
    end,
})

-- Highlight yanked text for 150ms
vim.api.nvim_create_autocmd("TextYankPost", {
    callback = function()
        vim.highlight.on_yank({ timeout = 150 })
    end
})

-- Python formatting
vim.api.nvim_create_autocmd("BufWritePost", {
    callback = function()
        local filetype = vim.bo.filetype
        if filetype == "python" then
            vim.cmd("silent !black %")
        else
            return
        end
    end
})

-- Markdown textwidth and colorcolumn
vim.api.nvim_create_autocmd("FileType", {
    callback = function()
        local filetype = vim.bo.filetype
        if filetype == "markdown" then
            -- vim.cmd("setlocal textwidth=80")
            vim.bo.textwidth = 80
            vim.o.colorcolumn = "80"
        else
            return
        end
    end
})

-- Change working directory to current file
vim.api.nvim_create_autocmd({"BufEnter", "TabEnter"},{
    callback = function()
        vim.cmd("silent! lcd %:p:h")
    end
})

